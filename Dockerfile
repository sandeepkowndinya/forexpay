FROM maven:3-alpine

RUN mkdir /app

WORKDIR /app

VOLUME /home/devops/.m2:/root/.m2

VOLUME /home/devops/.jenkins/workspace/docker-agent:/app

CMD mvn -B -DskipTests clean package -Dmaven.test.skip=true
